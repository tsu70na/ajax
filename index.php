<?php
include_once('db_access.php');
$datas=execute_sql('SELECT * FROM `dates`;');
$last_id=execute_sql('SELECT MAX(`id`) AS last_id FROM `dates`;');
$last_id=$last_id[0]['last_id'];
?>
<!DOCTYPE html>
<html>
<head>
<title></title>
<meta charset="utf-8">
<link rel="stylesheet" type="text/css" href="css/jquery-ui-1.9.2.custom.min.css">
</head>
<body>
    
<table id="mytable">
    <?php foreach ($datas as $data): ?>
<tr>
    <td><input type="text" class="datepicker" id="date_<?php echo $data['id']?>" value="<?php echo $data['date'] ?>"></td>
    <td><input type="button" class="save" data-id="<?php echo $data['id'] ?>" value="保存"></td>
</tr>
<?php endforeach;?>
</table>

<table style="display:none;">
<tr id="copy_tr">
    <td><input type="text" class="date" id=""></td>
    <td><input type="button" class="save" data-id="" value="保存"></td>
</tr>
</table>

<input type="button" value="行追加" class="btnAdd">

<hr>

<form id="form1">
    <input type="text" id="form1_id" name="id" value="">
    <input type="text" id="form1_date" name="date" value="">
</form>
<input type="button" value="Ajax" id="ajax">

<script src="js/jQuery.js"></script>
<script src="js/jquery-ui-1.9.2.custom.min.js"></script>
<script type="text/javascript">
jQuery(function(t){t.datepicker.regional.ja={closeText:"閉じる",prevText:"&#x3C;前",nextText:"次&#x3E;",currentText:"今日",monthNames:["1月","2月","3月","4月","5月","6月","7月","8月","9月","10月","11月","12月"],monthNamesShort:["1月","2月","3月","4月","5月","6月","7月","8月","9月","10月","11月","12月"],dayNames:["日曜日","月曜日","火曜日","水曜日","木曜日","金曜日","土曜日"],dayNamesShort:["日","月","火","水","木","金","土"],dayNamesMin:["日","月","火","水","木","金","土"],weekHeader:"週",dateFormat:"yy/mm/dd",firstDay:0,isRTL:!1,showMonthAfterYear:!0,yearSuffix:"年"},t.datepicker.setDefaults(t.datepicker.regional.ja)});

$(function(){
    $('.datepicker').datepicker();
    
    $('.btnClone').on('click', function(){
        //$('.btnClone').clone(true).insertAfter('.btnClone');
        $(this).clone(true).insertAfter(this);
    });
    
    
    $(".save").on('click', function(){
        var id = $(this).data('id');
        $('#form1_id').val(id);
        var date = $('#date_' + id).val();
        $('#form1_date').val(date);
    });

    $(".datepicker, .date").on('change', function(){
        var id = $(this).attr('id');   // 'date_*'
        id = id.replace(/date_/g, "");
             // 置き換えたい文字列を「/」で囲み、最後に g を付加する
        $('#form1_id').val(id);
        var date = $(this).val();
        $('#form1_date').val(date);
    });

    var next_id = <?php echo ($last_id + 1); ?>;
    $(".btnAdd").on('click', function(){
        var tr = $("#copy_tr").clone(true);
        tr.attr('id', '');
        tr.find('.date').attr('id', 'date_' + next_id);
        tr.find('.date').datepicker();
        tr.find('.save').data('id', next_id);
        tr.find('.save').attr('data-id', next_id);
        tr.appendTo("#mytable");
        
        next_id++;
    });

    // $("#ajax").on('click',function(){
        $(".datepicker, .date").on('change',function(){
        var id   = $('#form1_id').val();
        var date = $('#form1_date').val();
        $.ajax({
            url:"http://localhost/webprogram/06/ajax.php",
            type:"POST",
            data:{id: id, date: date},
            dataType:"json"
        }).done(function(data){
            alert("[" + data.status + "]" + data.id + ":" + data.date);;
            //alert("OK");
            //alert(data.status);
        }).fail(function(data){
            //alert("NG");
            //alert(data.status);
        });
    });
});
</script>
</body>
</html>